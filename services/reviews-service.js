/* eslint-disable max-len */
/* eslint-disable camelcase */
// import booksData from '../data/books-data.js';
// import reviewsData from '../data/reviews-data.js';
import serviceErrors from './../services/service-errors.js';

/**
 * Retrieves a book's review
 *
 * @function getBookReviewById
 * @param {object} reviewsData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {number} id The id of the book
 * @return {Promise<object>} The data from query or error
 */
const getBookReviewById = (reviewsData) => {
  return async (id) => {
    const existingReview = await reviewsData.getReviewsByBookId(id)
    if (!existingReview) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        existingReview: null
      }
    }
    return {
      error: null,
      existingReview: existingReview
    }
  }
};

/**
 * Retrieves all reviews of a particular book
 *
 * @function getAllReviewsForBook
 * @param {object} reviewsData The object which properties are functions to use
 * @param {object} booksData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {number} id The id of the book
 * @return {Promise<object>} The data from query or error
 */
const getAllReviewsForBook = (reviewsData, booksData) => {
  return async (id) => {
    const existingBook = await booksData.searchBy('id', id);
    if (!existingBook) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        allReviews: null
      }
    }
    const allReviews = await reviewsData.getBookReviewsById(id);
    return {
      error: null,
      allReviews: allReviews
    }
  }
};

/**
 * Creates a review for a particular book
 *
 * @function createReview
 * @param {object} reviewsData The object which properties are functions to use
 * @param {object} booksData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {number} bookId The id of the book
 * @param {string} content The content of the review
 * @param {number} userId The id of the user
 * @return {Promise<object>} The data from query or error
 */
const createReview = (reviewsData, booksData) => {
  return async (bookId, content, userId, userStatus) => {
    const existingBook = await booksData.searchBy('id', +bookId);
    const returnedBook = await reviewsData.getLoggedUserReturnedBook(+userId, +bookId);

    if (userStatus === 'Admin') {
      const newReview = await reviewsData.createReviewBy(content, userId, bookId);
      return {
        error: null,
        newReview: newReview
      }
    }
    if (!existingBook || !returnedBook[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        newReview: null
      }
    }
    const newReview = await reviewsData.createReviewBy(content, userId, bookId);
    return {
      error: null,
      newReview: newReview
    }
  }
};

/**
 * Updates a review of a particular book
 *
 * @function updateReview
 * @param {object} reviewsData The object which properties are functions to use
 * @param {object} booksData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {number} reviewId The id of the review
 * @param {number} userId The id of the user
 * @param {number} bookId The id of the book
 * @param {string} content The content of the review to be updated
 * @param {string} userRole The role of the user
 * @return {Promise<object>} The data from query or error
 */
const updateReview = (reviewsData, booksData) => {
  return async (reviewId, userId, bookId, content, userRole) => {
    // const existingBook = await booksData.searchBy('id', bookId);
    const existingReview = await reviewsData.getBookReview(+reviewId);
    // console.log(existingReview.users_id);

    if (!existingReview) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        updatedReview: null
      }
    }
    if (+existingReview.books_id === bookId &&
      (existingReview.users_id === userId || userRole === 'Admin')) {
      const updatedReview = await reviewsData.updateReviewBy(reviewId, content);

      return {
        error: null,
        updatedReview: updatedReview
      }
    }
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      updatedReview: null
    }
  }
};

/**
 * Deletes a particular review of a particular book
 *
 * @function deletingReview
 * @param {object} reviewsData The object which properties are functions to use
 * @param {object} booksData The object which properties are functions to use
 * @param {number} userRole The role of the user
 * @return
 * @async
 * @function
 * @param {number} bookId The id of the book
 * @param {number} userId The id of the user
 * @param {number} reviewId The id of the review to be deleted
 * @return {Promise<object>} The data from query or error
 */
const deletingReview = (reviewsData, booksData) => {
  return async (bookId, userId, reviewId, userRole) => {
    const existingBook = await booksData.searchBy('id', bookId);
    const existingReview = await reviewsData.getBookReview(reviewId);

    if (userRole === 'Admin' && existingBook && existingReview) {
      const deletedReview = await reviewsData.deleteReviewAdmin(reviewId);
      return {
        error: null,
        deletedReview: deletedReview
      }
    }
    if (!existingBook || !existingReview || existingReview.users_id !== userId) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        updatedReview: null
      }
    }

    const deletedReview = await reviewsData.deleteReview(reviewId, userId);
    // console.log(deletedReview);
    if (!deletedReview) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        deletedReview: null
      }
    }

    return {
      error: null,
      deletedReview: deletedReview
    }
  }
};

/**
 * Votes ffor a particular review of a particular book (like: 1 and dislike: -1)
 *
 * @function voteForReview
 * @param {object} reviewsData The object which properties are functions to use
 * @param {object} booksData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {number} bookId The id of the book
 * @param {number} reviewId The id of the review to be voted for
 * @param {number} userId The id of the user
 * @param {number} vote The vote to be given for a review
 * @return {Promise<object>} The data from query or error
 */
const voteForReview = (reviewsData) => {
  return async (bookId, reviewId, userId, vote) => {
    const checkReview = await reviewsData.reviewToBook(bookId, reviewId);
    if (!checkReview) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        updatedReview: null
      }
    }
    const checkUserVotes = await reviewsData.checkVotes(reviewId, userId);
    return checkUserVotes ?
      await reviewsData.updateVote(reviewId, userId, vote) :
      await reviewsData.vote(reviewId, userId, vote);
  }
}

const totalCountVotes = (reviewsData) => {
  return async (reviewId) => {
    const totalCount = await reviewsData.totalSumOfVotes(reviewId);
    return {
      error: null,
      total: totalCount
    }
  }
}

const allReviewsForAllBooks = (reviewsData) => {
  return async () => {
    const allReviews = await reviewsData.allBooksReviews();

    if (!allReviews) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        reviews: null
      }
    }

    return {
      error: null,
      reviews: allReviews
    }
  }
}
export default {
  getBookReviewById,
  getAllReviewsForBook,
  createReview,
  updateReview,
  deletingReview,
  voteForReview,
  totalCountVotes,
  allReviewsForAllBooks
};
