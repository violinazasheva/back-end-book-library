/* eslint-disable max-len */
/* eslint-disable camelcase */
import serviceErrors from '../services/service-errors.js';

/**
 * Retrieves all books that are NOT unlisted
 *
 * @function getAllBooks
 * @param {object} booksData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {string} filter The title or part of it
 * @return {Promise<object>} The data from query or error
 */
const getAllBooks = (booksData) => {
  return async (filter) => {
    return filter ?
      await booksData.searchBy('title', filter) :
      await booksData.getAll();
  }
};

/**
 * Returns object with needed data or error
 *
 * @function viewBookById
 * @param {object} booksData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {number} id The id of the book
 * @return {Promise<object>} The data from query or error
 */
const viewBookById = (booksData) => {
  return async (id) => {
    const book = await booksData.getBookBy('id', id);
    if (!book) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        book: null
      }
    }
    return { error: null, book: book }
  }
};

/**
 * Checks if the book with id exist and borrows the book
 *
 * @function borrowBookById
 * @param {object} booksData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {number} id The id of the book to borrow
 * @param {number} userId The id of the user, who will borrow the book
 * @return {Promise<object>} The data from query or error
 */
const borrowBookById = (booksData) => {
  return async (id, userId) => {
    const existingBook = await booksData.getBookBy('id', id);
    if (existingBook && existingBook.status === 'free') {
      const borrowedBook = await booksData.borrowBook(id, +userId);
      return {
        error: null,
        borrowedBook: await booksData.getBookBy('id', id)
      }
    }
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      borrowedBook: null
    }
  }
};

/**
 * Checks if the book with id exist and returns the book
 *
 * @function returnBookById
 * @param {object} booksData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {number} bookId The id of the book to return
 * @param {number} userId The id of the user, who will return the book
 * @return {Promise<object>} The data from query or error
 */
const returnBookById = (booksData) => {
  return async (bookId, userId) => {
    const existingBook = await booksData.getBookBy('id', bookId);
    if (existingBook && existingBook.status === 'borrowed' && +existingBook.curr_reader_id === userId) {
      const returnedBook = await booksData.returnBook(bookId);
      // sets on table returned_books which user had returned a curren book
      const setReturn = await booksData.setReturnedFromUser(userId, bookId);
      // console.log(existingBook);
      return {
        error: null,
        returnedBook: existingBook
      }
    }
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      returnedBook: null
    }
  }
};

/**
 * Adds new book to database
 *
 * @function createBook
 * @param {object} booksData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {object} bookFromBody The object with author id and title of the book to create
 * @param {number} usersId The id of the user, who will create the book // this is allowed only for ADMIN
 * @return {Promise<object>} The data from query or error
 */
const createBook = (booksData) => {
  return async (data, users_id) => {
    const { title, author, url } = data;
    const existingBook = await booksData.getBookBy('title', title);
    const defaultCoverUrl =
      'https://image.freepik.com/free-photo/book-library-with-open-textbook_1150-5923.jpg';

    if (existingBook) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        newBook: null
      }
    }

    const newBook = await booksData.createBookBy(
        title,
        author,
        users_id,
        url || defaultCoverUrl
    );

    return {
      error: null,
      newBook: newBook,
    };
  }
};

/**
 * Checks if the book with id exist and deletes the book
 *
 * @function deleteBook
 * @param {object} booksData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {number} bookId The id of the book to delete
 * @return {Promise<object>} The data from query or error
 */
const deleteBook = (booksData) => {
  return async (bookId) => {
    const existingBook = await booksData.getBookBy('id', bookId);

    if (!existingBook) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        newBook: null
      }
    }

    const bookToDelete = await booksData.deleteBook(bookId);
    return {
      error: null,
      newBook: bookToDelete,

    }
  }
};

export default {
  getAllBooks,
  viewBookById,
  borrowBookById,
  returnBookById,
  createBook,
  deleteBook
};
