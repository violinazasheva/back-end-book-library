/* eslint-disable max-len */
import serviceErrors from '../services/service-errors.js';

/**
 * Retrieves a rating by id
 *
 * @function getRatingById
 * @param {object} booksData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {number} id The id of the rating
 * @return {Promise<object>} The data from query or error
 */
const getRatingById = (booksData) => {
  return async (id) => {
    const book = await booksData.getBookBy('id', +id);
    const rating = await booksData.getRatingByBookId(+id);
    if (!book || !rating) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        book: null,
        rating: null
      }
    }
    return {
      error: null,
      book: book,
      rating: rating
    }
  }
};

/**
 * Creates a rating for a particular book
 *
 * @function createReview
 * @param {object} booksData The object which properties are functions to use
 * @param {object} reviewsData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {number} bookId The id of the book
 * @param {string} star The star to be given in the range [1...5]
 * @param {number} userId The id of the user
 * @return {Promise<object>} The data from query or error
 */
const createRating = (booksData, reviewsData) => {
  return async (bookId, star, userId) => {
    const bookRevs = await reviewsData.getReviewsByUserIDandBookID(+userId, +bookId);
    if (!bookRevs) {
      return {
        error: serviceErrors.INVALID_RATE,
        newRating: null
      }
    }
    const prevRating = await reviewsData.checkRating(userId, bookId);
    if (prevRating) {
      const updatingRating = await reviewsData.updateRating(prevRating.id, userId, star);
      return {
        error: null,
        newRating: 'Rating is updated'
      }
    }
    const newRating = await booksData.createRatingBy(star, bookId, userId);
    return {
      error: null,
      newRating: newRating
    }
  }
};

export default {
  getRatingById,
  createRating
};
