/* eslint-disable max-len */
import serviceErrors from './service-errors.js';
import bcrypt from 'bcrypt';
import { DEFAULT_USER_ROLE } from './../config.js';

/**
 * Creates and adds new user to the database
 *
 * @function createUser
 * @param {object} usersData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {object} userCreate The object with the username and password of the new user
 * @return {Promise<object>} The data from query or error
 */
const createUser = (usersData) => {
  return async (userCreate) => {
    const { username, password } = userCreate;

    const existingUser = await usersData.getBy('username', username);

    if (existingUser) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        user: null
      }
    }

    const passwordHash = await bcrypt.hash(password, 10);
    const user = await usersData.create(
        username,
        passwordHash,
        DEFAULT_USER_ROLE
    );

    return { error: null, user: user };
  }
};

/**
 * Checks if the username and password are correct and varifies the user
 *
 * @function signInUser
 * @param {object} usersData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {string} username The username of the user
 * @param {string} password The password of the user
 * @return {Promise<object>} The data from query or error
 */
const signInUser = (usersData) => {
  return async (username, password) => {
    const user = await usersData.getWithRole(username);

    if (!user || !(await bcrypt.compare(password, user.password)) || user.is_deleted === 1) {
      return {
        error: serviceErrors.INVALID_SIGNIN,
        user: null
      }
    }

    return {
      error: null,
      user: user
    }
  }
};

/**
 * Checks if user with given id exists
 *
 * @function getUserById
 * @param {object} usersData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {number} id The id of the user
 * @return {Promise<object>} The data from query or error
 */
const getUserById = (usersData) => {
  return async (id) => {
    const user = await usersData.getBy('id', id);

    if (!user) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null
      };
    }

    return { error: null, user: user };
  }
};

/**
 * Updates ban status of the user
 *
 * @function updateBanStatus
 * @param {object} usersData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {number} userId The id of the user
 * @param {number} banStatus [-1] or [1]
 * @return {Promise<object>} The data from query or error
 */
const updateBanStatus = (usersData) => {
  return async (userId, banStatus) => {
    const changeStatus = await usersData.changeBanStatus(userId, banStatus);
    const userInfo = await usersData.getFullInfoById(userId);

    if (!userInfo) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null
      };
    }

    return { error: null, user: userInfo };
  }
};

/**
 * Set user's property is_deleted = 1
 *
 * @function deletingUser
 * @param {object} usersData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {number} userId The id of the user
 * @return {Promise<object>} The data from query or error
 */
const deletingUser = (usersData) => {
  return async (userId) => {
    const existingUser = await usersData.getBy('id', userId);
    const currReader = await usersData.getCurrReaderId(userId);

    if (currReader || !existingUser || existingUser.is_deleted === 1) {
      return {
        error: serviceErrors.OPERATION_NOT_PERMITTED,
      }
    }
    const deleteOperation = await usersData.deleteUser(userId);
    return { error: null };
  }
};

const getAllUsers = (usersData) => {
  return async () => {
    const allUsers = await usersData.allUsers();

    if (!allUsers) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        allUsers: null
      };
    }

    return { error: null, allUsers: allUsers };
  }
};

export default {
  createUser,
  signInUser,
  getUserById,
  updateBanStatus,
  deletingUser,
  getAllUsers
};
