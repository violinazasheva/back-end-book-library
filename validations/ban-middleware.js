import usersData from '../data/users-data.js'

export const banMiddleware = (banStatus) => {
  return async (req, res, next) => {
    const userId = req.user.id;
    const checkStatus = await usersData.getFullInfoById(userId);

    if (checkStatus.ban_status === banStatus) {
      next();
    } else {
      res.status(403).send({
        message: 'You do not have access to this resource'
      })
    }
  }
};

// working on ban status middleware
