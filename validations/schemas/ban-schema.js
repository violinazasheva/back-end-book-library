export const banSchema = {
  banStatus: (int) => {
    if (typeof int !== 'number') {
      return 'Ban status should be a number';
    }
    if (int !== 1 && int !== 0) {
      return 'Ban status should be an integer 0 or 1'
    }
    return null;
  }
}
