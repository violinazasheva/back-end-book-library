export const createBookSchema = {
  title: (value) => {
    if (!value) {
      return 'Title is required';
    }

    if (typeof value !== 'string' || value.length < 3 || value.length > 25) {
      return 'Title should be a string in range [3..25]';
    }

    return null;
  },
  author: (value) => {
    if (!value) {
      return 'Author is required';
    }

    if (typeof value !== 'string' || value.length < 3 || value.length > 25) {
      return 'Author should be a string in range [3..25]';
    }

    return null;
  }
};
