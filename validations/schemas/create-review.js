export const createReviewSchema = {
  content: (value) => {
    if (!value) {
      return 'Content is required';
    }

    if (typeof value !== 'string' || value.length < 3 || value.length > 25) {
      return 'Content should be a string in range [3..25]';
    }

    return null;
  }
};
