export const createRatingSchema = {
  stars: (value) => {
    if (!value) {
      return 'Rating star is required.';
    }

    if (typeof value !== 'number' || value < 1 || value > 5) {
      return 'Rating should be a star in range [1..5]';
    }

    return null;
  }
};

export default createRatingSchema;
