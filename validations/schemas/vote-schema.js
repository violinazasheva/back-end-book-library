export const voteSchema = {
  vote: (int) => {
    if (int === 1 || int === -1) {
      return null;
    }
    return 'You can vote only with numbers: [1] or [-1]';
  }
}
