import passport from 'passport';
import { JWT_STRATEGY_NAME } from './../config.js';

// Authentication middleware
const authMiddleware = passport.authenticate(
    JWT_STRATEGY_NAME,
    { session: false }
);

// Authorization middleware
const roleMiddleware = (roleName) => {
  return (req, res, next) => {
    if (req.user && (req.user.role === roleName || 'Amin')) {
      next()
    } else {
      res.status(403).send({
        message: 'This resource is forbidden.'
      })
    }
  };
};


export {
  authMiddleware,
  roleMiddleware
};
