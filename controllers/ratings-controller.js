/* eslint-disable max-len */
/* eslint-disable new-cap */

import express from 'express';
import ratingService from '../services/ratings-service.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import { createValidator, createRatingSchema, banMiddleware } from '../validations/index.js';
import serviceErrors from '../services/service-errors.js';
import reviewsData from '../data/reviews-data.js';
import booksData from '../data/books-data.js';


const ratingsController = express.Router();

ratingsController

// Get average rating by bookId
    .get('/:id/ratings',
        authMiddleware,
        roleMiddleware('User'),
        banMiddleware(0),
        async (req, res) => {
          const { id } = req.params;
          const { error, rating, book } = await ratingService.getRatingById(booksData)(+id);
          if (error) {
            return res.status(400).send({
              message: `Not a valid book.`
            });
          }
          book.average_rating = rating[0].avgRating;
          return res.status(200).send(book);
        })

// Create a book rating
    .post('/:id/ratings',
        authMiddleware,
        roleMiddleware('User'),
        banMiddleware(0),
        createValidator(createRatingSchema),
        async (req, res) => {
          const { stars } = req.body;
          const { id } = req.params;

          const userId = req.user.id;
          const { error, newRating } = await ratingService.createRating(booksData, reviewsData)(+id, +stars, +userId);
          if (error === serviceErrors.RECORD_NOT_FOUND) {
            return res.status(400).send({ message: 'The book is not found' });
          } else if (error === serviceErrors.INVALID_RATE) {
            return res.status(400).send({ message: 'Write a review before rating.' });
          }
          return res.status(200).send({ newRating: newRating });
        });


export default ratingsController;
