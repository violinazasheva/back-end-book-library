/* eslint-disable max-len */
import express from 'express';
import booksData from '../data/books-data.js';
import bookService from '../services/books-service.js';
import { banMiddleware } from '../validations/index.js';

import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';

// eslint-disable-next-line new-cap
const booksController = express.Router();

booksController

    .get('/',
        authMiddleware,
        roleMiddleware('User'),
        async (req, res) => {
          const { title } = req.query;
          const books = await bookService.getAllBooks(booksData)(title);
          res.status(200).send(books);
        })

    .get('/:id',
        authMiddleware,
        roleMiddleware('User'),
        async (req, res) => {
          const { id } = req.params;

          const { error, book } = await bookService.viewBookById(booksData)(+id);
          if (error) {
            return res.status(404).send({
              message: 'The book is not found!'
            });
          }

          return res.status(200).send(book);
        })


// Borrow a book
    .post('/:id',
        authMiddleware,
        roleMiddleware('User'),
        // banMiddleware(0),
        async (req, res) => {
          const userId = req.user.id;
          const { id } = req.params;

          const { error, borrowedBook } = await bookService.borrowBookById(booksData)(+id, userId);
          if (error) {
            res.status(404).send({ message: 'The book cannot be borrowed.' });
          } else {
            res.status(200).send({ message: 'The book was successfully borrowed!', borrowedBook: borrowedBook });
          }
        })

// Return a book
    .delete('/:id',
        authMiddleware,
        roleMiddleware('User'),
        // banMiddleware(0),
        async (req, res) => {
          const { id } = req.params;
          const userId = req.user.id;

          const { error, returnedBook } = await bookService.returnBookById(booksData)(+id, userId);
          if (error) {
            res.status(404).send({ message: 'The book cannot be returned or it is already free.' });
          } else {
            returnedBook.status = 'free';
            res.status(200).send({ message: 'The book was successfully returned!', returnedBook: returnedBook });
          }
        })

export default booksController;
