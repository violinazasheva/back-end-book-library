/* eslint-disable camelcase */
/* eslint-disable max-len */
import express from 'express';

import booksData from '../data/books-data.js';
import reviewsData from '../data/reviews-data.js';
import usersData from '../data/users-data.js';

import bookService from '../services/books-service.js';
import reviewsService from '../services/reviews-service.js';
import userService from '../services/users-service.js';

import { createValidator, createBookSchema, createReviewSchema, banSchema } from '../validations/index.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import serviceErrors from '../services/service-errors.js';
import usersService from '../services/users-service.js';


// eslint-disable-next-line new-cap
const adminController = express.Router();

adminController

// Get all users
    .get('/users',
        authMiddleware,
        roleMiddleware('Admin'),
        async (req, res) => {
          const { error, allUsers } = await usersService.getAllUsers(usersData)();

          if (error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Users are not found!' });
          } else {
            res.status(200).send(allUsers);
          }
        })

// see all books
    .get('/books', authMiddleware, roleMiddleware('Admin'), async (req, res) => {
      const { search } = req.query;
      const allBooks = await bookService.getAllBooks(booksData)(search);

      return res.status(200).send(allBooks);
    })

// view individual book
    .get('/books/:id', authMiddleware, roleMiddleware('Admin'), async (req, res) => {
      const { id } = req.params;

      const { error, book } = await bookService.viewBookById(booksData)(+id);
      if (error) {
        return res.status(404).send({
          message: 'The book is not found!'
        });
      }

      return res.status(200).send(book);
    })

// create new book
    .post('/books', authMiddleware, roleMiddleware('Admin'), createValidator(createBookSchema),
        async (req, res) => {
          const book = req.body;
          const users_id = req.user.id;

          const { error, newBook } = await bookService.createBook(booksData)(
              book,
              +users_id
          );

          if (error) {
            res.status(409).send({ message: 'Book already exists in the library.' });
          } else {
            // prints the added book
            res.status(201).send({ message: 'New book was created seccessfully.', newBook: newBook });
          }
        })

// delete book
    .delete('/books/:id', authMiddleware, roleMiddleware('Admin'), async (req, res) => {
      const { id } = req.params;
      const { error, book } = await bookService.deleteBook(booksData)(+id);
      if (error) {
        return res.status(400).send({ message: 'The book is not found or already deleted' });
      }

      return res.status(200).send({ message: 'Book is deleted successsully' });
    })

// get all reviews for book with id
    .get('/:id/reviews', authMiddleware, roleMiddleware('Admin'), async (req, res) => {
      const { id } = req.params;

      const { error, allReviews } = await reviewsService.getAllReviewsForBook(reviewsData, booksData)(id);
      if (error) {
        return res.status(400).send({ message: 'The book is not found' });
      }
      return res.status(200).send({ allReviews: allReviews });
    })

// get all reviews for all books
    .get('/reviews', authMiddleware, roleMiddleware('Admin'), async (req, res) => {
      const { error, reviews } = await reviewsService.allReviewsForAllBooks(reviewsData)();
      if (error) {
        return res.status(400).send({ message: 'The book is not found' });
      }

      return res.status(200).send(reviews);
    })

// update review with id about a book with id
    .put('/:id/reviews/:reviewId', authMiddleware, roleMiddleware('Admin'), createValidator(createReviewSchema), async (req, res) => {
      const { id, reviewId } = req.params;
      const userId = req.user.id;
      const userRole = req.user.role;
      const content = req.body.content;
      // console.log(content);
      const { error, updatedReview } = await reviewsService.updateReview(reviewsData)(+reviewId, +userId, +id, content, userRole)

      if (error) {
        return res.status(400).send({ message: 'Something went wrong' });
      }
      return res.status(200).send({ message: 'Review is updated successfully' });
    })

// BAN USER or UNBAN USER
    .put('/users/:id/banstatus', authMiddleware, roleMiddleware('Admin'), createValidator(banSchema), async (req, res) => {
      const { id } = req.params;
      const { banStatus } = req.body;
      const { error, user } = await userService.updateBanStatus(usersData)(+id, banStatus);
      if (error) {
        return res.status(400).send({ message: 'User is not found' });
      }
      return res.status(200).send({ message: 'Ban status updated', user: user });
    })

// delete user
    .delete('/users/:id', authMiddleware, roleMiddleware('Admin'), async (req, res) => {
      const { id } = req.params;

      const { error } = await userService.deletingUser(usersData)(+id);

      if (error) {
        res.status(404).send({ message: 'User cannot be deleted. Either a reader or not-existing.' });
      } else {
        res.status(200).send({ message: `User with id ${id} is deleted.` });
      }
    });

export default adminController;
