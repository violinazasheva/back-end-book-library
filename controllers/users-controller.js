/* eslint-disable new-cap */
/* eslint-disable max-len */
import express from 'express';
import usersData from '../data/users-data.js';
import usersService from '../services/users-service.js';
import serviceErrors from '../services/service-errors.js';
import { createUserSchema, createValidator, banMiddleware } from '../validations/index.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';

const usersController = express.Router();


usersController

// Get user by ID
    .get('/:id',
        authMiddleware,
        roleMiddleware('User'),
        banMiddleware(0),
        async (req, res) => {
          console.log(req.user)
          const { id } = req.params;

          const { error, user } = await usersService.getUserById(usersData)(+id);

          if (error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'User not found!' });
          } else {
            res.status(200).send(user);
          }
        })

// Create a user
    .post('/', createValidator(createUserSchema), async (req, res) => {
      const createData = req.body;
      const { error, user } = await usersService.createUser(usersData)(createData);
      if (error === serviceErrors.DUPLICATE_RECORD) {
        return res.status(409).send({ message: 'Username is not available' });
      }
      return res.status(201).send(user);
    });

export default usersController;

