/* eslint-disable max-len */
import express from 'express';
import reviewsData from './../data/reviews-data.js'
import reviewsService from './../services/reviews-service.js';
import { createValidator, createReviewSchema, banMiddleware, voteSchema } from '../validations/index.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import booksData from '../data/books-data.js';


// eslint-disable-next-line new-cap
const reviewsController = express.Router();

reviewsController

// Read all reviews for a book with ID
    .get('/:id/reviews',
        authMiddleware,
        roleMiddleware('User'),
        banMiddleware(0),
        async (req, res) => {
          const { id } = req.params;
          const { error, allReviews } = await reviewsService.getAllReviewsForBook(reviewsData, booksData)(+id);
          if (error) {
            return res.status(400).send({ message: 'The book is not found' });
          }
          return res.status(200).send(allReviews);
        })

// Create a book review
    .post('/:id/reviews',
        authMiddleware,
        roleMiddleware('User'),
        banMiddleware(0),
        createValidator(createReviewSchema),
        async (req, res) => {
          const { content } = req.body;
          const { id } = req.params;
          const userId = req.user.id;
          const userStatus = req.user.role;

          const { error, newReview } = await reviewsService.createReview(reviewsData, booksData)(id, content, userId, userStatus);
          if (error) {
            return res.status(400).send({ message: 'The book was not borrowed!' });
          }
          return res.status(200).send({ newReview: newReview });
        })

// Update a book review
    .put('/:id/reviews/:reviewId',
        authMiddleware,
        roleMiddleware('User'),
        banMiddleware(0),
        createValidator(createReviewSchema),
        async (req, res) => {
          const { id, reviewId } = req.params;
          const userId = req.user.id;
          const userRole = req.user.role;
          const content = req.body.content;

          const { error, updatedReview } = await reviewsService.updateReview(reviewsData, booksData)(+reviewId, +userId, +id, content, userRole)

          if (error) {
            return res.status(400).send({ message: 'Something went wrong' });
          }
          return res.status(200).send({ message: 'Review is updated successfully' });
        })

// Delete a review
    .delete('/:id/reviews/:reviewId',
        authMiddleware,
        roleMiddleware('User' || 'Admin'),
        banMiddleware(0),
        async (req, res) => {
          const { id, reviewId } = req.params;
          const userId = req.user.id;
          const userRole = req.user.role;

          const { error, deletedReview } = await reviewsService.deletingReview(reviewsData, booksData)(+id, userId, +reviewId, userRole);

          if (error) {
            return res.status(400).send({ message: 'Something went wrong' });
          }
          return res.status(200).send({ message: 'Review is deleted successfully' });
        })

// Like review
    .put('/:id/reviews/:reviewId/votes',
        authMiddleware,
        roleMiddleware('User'),
        banMiddleware(0),
        createValidator(voteSchema),
        async (req, res) => {
          const { id, reviewId } = req.params;
          const userId = req.user.id;
          const { vote } = req.body;

          const voting = await reviewsService.voteForReview(reviewsData)(+id, +reviewId, userId, vote);
          const { error, total } = await reviewsService.totalCountVotes(reviewsData)(+reviewId);

          if (!total || voting.error) {
            return res.status(400).send({ message: 'Voting for this review failed' });
          }
          return res.status(200).send({ message: 'Your vote is accepted', likes: total.likes, dislikes: total.dislikes });
        })
export default reviewsController;
