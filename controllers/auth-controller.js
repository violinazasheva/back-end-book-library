/* eslint-disable new-cap */
/* eslint-disable max-len */
import express from 'express';
import usersService from '../services/users-service.js';
import serviceErrors from '../services/service-errors.js';
import createToken from '../auth/create-token.js';
import usersData from '../data/users-data.js';
import { authMiddleware } from '../auth/auth-middleware.js';

const authController = express.Router();

authController
// SIGN IN
    .post('/signin', async (req, res) => {
      const { username, password } = req.body;
      const { error, user } = await usersService.signInUser(usersData)(username, password);

      if (error === serviceErrors.INVALID_SIGNIN) {
        return res.status(400).send({
          message: 'Invalid username/password'
        })
      }
      const payload = {
        sub: user.id,
        username: user.username,
        role: user.role,
        banStatus: user.ban_status,
      };
      const token = createToken(payload);
      return res.status(200).send({
        token: token
      });
    })

// LOGOUT
    .delete('/signout', authMiddleware, async (req, res) => {
      const username = req.user.username;
      return res.status(200).send({ message: `${username} has signed out successfully.` });
    })

export default authController;
