/* eslint-disable camelcase */
/* eslint-disable no-undef */
/* eslint-disable max-len */
import pool from './pool.js';

/**
 * Returns a  review by a certain parameter
 *
 * @async
 * @function getBookReview
 * @param {number} revId The value, for which will be searched in column
 * @return {object} The object with searched data
 */
const getBookReview = async (revId) => {
  const sql = `
  SELECT *
  FROM reviews
  WHERE id = ${revId}
  `;

  const result = await pool.query(sql);
  return result[0];
};

/**
 * Returns a review by id
 *
 * @async
 * @function getBookReviewsById
 * @param {number} bookId The bookId, for which will be searched the review
 * @return {object} The object with searched data
 */
const getBookReviewsById = async (bookId) => {
  const sql = `
  select b.url, 
  b.id as bookId, 
  u.username, 
  r.content, 
  r.id as reviewId,
  (select COUNT(*) AS likes from votes WHERE reviews_id = r.id AND type = 1) as likes,
  (select COUNT(*) AS dislikes from votes WHERE reviews_id = r.id AND type = -1) as dislikes
  from books b
  join reviews r
  on  r.books_id = b.id
  join users u
  on r.users_id = u.id
  where b.id = ?
  and r.is_deleted = 0;
  `;

  const result = await pool.query(sql, [bookId]);
  const reviews = [];
  for (const review of result) {
    reviews.push(review);
  }

  return reviews;
};

/**
 * Returns reviews by book's id
 *
 * @async
 * @function getReviewsByBookId
 * @param {number} bookId The bookId, for which will be searched the review
 * @return {object} The object with searched data
 */
const getReviewsByBookId = async (bookId) => {
  const sql = `
  select a.name as author, b.title, r.content, r.id as review_id, st.status
  from books b
  join author a
  on a.id = b.author_id
  join status st
  on st.id = b.status_id
  join reviews r
  on  r.books_id = b.id
  where b.id = ?
  and r.is_deleted = 0;
  `;
  const result = await pool.query(sql, [bookId]);
  return result;
};

/**
 * Returns reviews by a particular user's id and book's id
 *
 * @async
 * @function getReviewsByUserIDandBookID
 * @param {number} userId The userId, for which will be searched the review
 * @param {number} bookId The bookId, for which will be searched the review
 * @return {object} The object with searched data
 */
const getReviewsByUserIDandBookID = async (userId, bookId) => {
  const sql = `
  select * 
  from reviews r
  where r.users_id = ?
  and r.books_id = ?;
  `;

  const result = await pool.query(sql, [userId, bookId]);
  return result[0];
};

/**
 * Creates review from a particular user for a particular book
 *
 * @async
 * @function createReviewBy
 * @param {string} content The content of the review
 * @param {number} usersId The userId which will write the review
 * @param {number} bookId The bookId, for which will be written the review
 * @return {object} The object with created data
 */
const createReviewBy = async (content, usersId, bookId) => {
  const sql = `
  INSERT INTO mydb.reviews(content, users_id, books_id) 
  VALUES (?, ?, ?);
  `;

  const result = await pool.query(sql, [content, usersId, bookId]);

  return {
    content: content,
    users_id: usersId,
    books_id: bookId,
  };
};

/**
 * Updates a review with a new content
 *
 * @async
 * @function updateReviewBy
 * @param {number} id The id of the review which will be updated
 * @param {string} newContent The new review's content
 * @return {object} The object with updated data
 */
const updateReviewBy = async (id, newContent) => {
  const sql = `
  UPDATE reviews
  SET content = "${newContent}"
  WHERE id = ?;
  `;

  return await pool.query(sql, [id]);
};

/**
 * Returns a review by its id
 *
 * @async
 * @function getReviewById
 * @param {number} id The id of the review which will be returned
 * @return {object} The object with searched data
 */
const getReviewById = async (id) => {
  const sql = `
  select * 
  from reviews
  where id = ?;`;

  return await pool.query(sql, [+id]);
};

/**
 * Deletes a review by its id
 *
 * @async
 * @function deleteReview
 * @param {number} reviewId The id of the review which will be deleted
 * @param {number} userId The id of the user who wrote the review
 * @return {object} The object with deleted(updated) data
 */
const deleteReview = async (reviewId, userId) => {
  const sql = `
  update reviews r
  set r.is_deleted = 1
  where r.id = ? and r.users_id = ?;`;

  return await pool.query(sql, [reviewId, userId]);
};

const deleteReviewAdmin = async (reviewId) => {
  const sql = `
  update reviews r
  set r.is_deleted = 1
  where r.id = ?`;

  return await pool.query(sql, [reviewId]);
};

/**
 * Returns the returned book of the logged user
 *
 * @async
 * @function getLoggedUserReturnedBook
 * @param {number} userId The userId that has a returned book
 * @param {number} bookId The bookId which is returned
 * @return {object} The object with searched data
 */
const getLoggedUserReturnedBook = async (userId, bookId) => {
  const sql = `
    select *
    from returned_books
    where users_id = ? and books_id = ?;
  `;

  return await pool.query(sql, [+userId, +bookId]);
};

/**
 * Checks the votes of a particular review by a particular user
 *
 * @async
 * @function checkVotes
 * @param {number} reviewId The reviewId of which we will check the votes
 * @param {number} userId The bookId which is returned
 * @return {object} The object with searched data
 */
const checkVotes = async (reviewId, userId) => {
  const sql = `
  select * from votes v
  where v.reviews_id = ? and v.users_id = ?;
  `;
  const result = await pool.query(sql, [reviewId, userId]);
  return result[0];
};

/**
 * Votes for a particular review (like: 1 or dislike: -1)
 *
 * @async
 * @function vote
 * @param {number} reviewId The id of the review
 * @param {number} userId The id of the user
 * @param {number} vote The vote of the review
 * @return {object} The object, which contains "reviewId", "userId" and "vote"
 */
const vote = async (reviewId, userId, vote) => {
  const sql = `
  INSERT INTO mydb.votes 
  (type, reviews_id, users_id) 
  VALUES (?, ?, ?);
  `;

  const result = await pool.query(sql, [vote, reviewId, userId]);
  return result;
};

/**
 * Updates the vote for a particular review
 *
 * @async
 * @function updateVote
 * @param {number} reviewId The id of the review
 * @param {number} userId The id of the user
 * @param {number} vote The new vote of the review
 * @return {object} The object, which contains "reviewId", "userId" and "vote"
 */
const updateVote = async (reviewId, userId, vote) => {
  const sql = `
  UPDATE mydb.votes 
  SET type = ? 
  WHERE reviews_id = ? 
  AND users_id = ?;
  `;

  const result = await pool.query(sql, [vote, reviewId, userId]);
  return result;
}

/**
 * Returns a review of a particular book
 *
 * @async
 * @function reviewToBook
 * @param {number} bookId The id of the book
 * @param {number} reviewId The id of the review
 * @return {object} The object with searched data
 */
const reviewToBook = async (bookId, reviewId) => {
  const sql = `
  select * 
  from reviews
  where id = ?
  and books_id = ?
  and is_deleted = 0;
  `;

  const result = await pool.query(sql, [reviewId, bookId]);
  return result[0];
};

/**
 * Checks if the user has already rated the book
 *
 * @async
 * @function checkRating
 * @param {number} userId The id of the user
 * @param {number} bookId The id of the book
 * @return {object} The object with searched data
 */
const checkRating = async (userId, bookId) => {
  const sql = `
  select * 
  from ratings r
  where r.users_id = ?
  and r.books_id = ?;
  `;
  const result = await pool.query(sql, [userId, bookId]);
  return result[0];
};

/**
 * Updates previous rating of the user
 *
 * @async
 * @function updateRating
 * @param {number} rateId The id of the rating
 * @param {number} userId The id of the user
 * @param {number} star The rating of the user
 * @return {object} The object with searched data
 */
const updateRating = async (rateId, userId, star) => {
  const sql = `
  UPDATE ratings 
  SET stars = ? 
  WHERE id = ?
  and users_id = ?;
  `;
  const result = await pool.query(sql, [star, rateId, userId]);
  return result;
}

const totalSumOfVotes = async (reviewId) => {
  const sql = `
  select sum(type = 1) AS likes,
  sum(type = -1) AS dislikes
  from votes
  where reviews_id = ?;
  `;
  const result = await pool.query(sql, [reviewId]);
  return result[0];
}

const allBooksReviews = async () => {
  const sql = `
  select b.id as bookId, b.title, r.id as reviewId, r.content, u.id as userId, u.username, r.is_deleted as isDeleted
  from reviews r
  join books b
  on r.books_id = b.id
  join users u
  on r.users_id = u.id
  order by b.id;
  `;
  const result = await pool.query(sql);

  const reviews = [];
  for (const review of result) {
    reviews.push(review);
  }

  return reviews;
}
export default {
  getBookReview,
  getReviewsByBookId,
  getBookReviewsById,
  getReviewsByUserIDandBookID,
  createReviewBy,
  updateReviewBy,
  getReviewById,
  deleteReview,
  deleteReviewAdmin,
  getLoggedUserReturnedBook,
  checkVotes,
  vote,
  updateVote,
  reviewToBook,
  checkRating,
  updateRating,
  totalSumOfVotes,
  allBooksReviews
};
