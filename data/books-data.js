/* eslint-disable camelcase */
/* eslint-disable max-len */
import pool from './pool.js';

/**
 * Retrieves all books that are NOT unlisted
 *
 * @async
 * @function getAll
 * @return {Promise<object>} The data from query
 */
const getAll = async () => {
  const sql = `
  select b.id, b.url, a.name as author, b.title, st.status
  from books b
  join author a
  on a.id = b.author_id
  join status st
  on st.id = b.status_id
  where st.id != 2; 
  `;
  // id = 2 is id of unlisted
  const result = await pool.query(sql);
  return result;
};

/**
 * Keep the record of the returned books
 *
 * @async
 * @function setReturnedFromUser
 * @param {number} userId The id of the user, that returns the book
 * @param {number} bookId The id of the book, that is returned
 * @return {Promise<object>} The data from query
 */
const setReturnedFromUser = async (userId, bookId) => {
  const sql = `
  INSERT INTO returned_books (users_id, books_id) 
  VALUES (?, ?);
  `;
  const result = await pool.query(sql, [userId, bookId]);
  return result;
};

/**
 * Searches books by given parameters
 *
 * @async
 * @function searchBy
 * @param {string} column The name of the column
 * @param {string/number} value Searched value in the column
 * @return {Promise<object>} The data from query
 */
const searchBy = async (column, value) => {
  const sql = `
  select b.id, b.url, a.name as author, b.title, st.status, b.curr_reader_id
  from books b
  join author a
  on a.id = b.author_id
  join status st
  on st.id = b.status_id
  where b.${column} like '%${value}%'
  and b.status_id != 2;
  `;
  const result = await pool.query(sql);
  // eslint-disable-next-line consistent-return
  const res = result.map((book) => {
    if (book.id) {
      return book;
    }
  })
  return res;
};

/**
 * Adds new author
 *
 * @async
 * @function addAuthor
 * @param {string} authorNname The name of the author to add
 * @return {object} The object with "id" and "name" of the added author
 */
const addAuthor = async (authorNname) => {
  const sql = `
  INSERT INTO author (name)
  VALUES (?);
  `;
  const result = await pool.query(sql, [authorNname]);
  return {
    id: result.insertId,
    name: result.name
  };
};

// /** TO BE EDITED
//  * Adds new book to the database
//  *
//  * @async
//  * @function createBookBy
//  * @param {number} authorId The id of the author
//  * @param {string} title The title of the book
//  * @param {number} userId The id of the user, that adds the book
//  * @return {object} The object, which contains "id", "title" and "author_id"
//  */
const createBookBy = async (title, author, users_id, url) => {
  const insertAuthorIfNotExists = `
INSERT INTO author(Name)
        SELECT * FROM (SELECT '${author}') AS tmp
        WHERE NOT EXISTS (
            SELECT name FROM author WHERE name = '${author}'
        ) LIMIT 1;
    `;

  const newAuthor = await pool.query(insertAuthorIfNotExists, [author]);

  if (!newAuthor.insertId) {
    const idQuery = await pool.query(`
        SELECT sum(id) 
        FROM author
        WHERE Name = '${author}'
        `);

    const id = idQuery[0]['sum(id)'];

    const sql = `
        INSERT INTO books(title, author_id, users_id, url)
        VALUES (?, ?, ?, ?);
    `;

    const result = await pool.query(sql, [title, id, users_id, url]);

    return {
      id: result.insertId,
      title: title,
      author: author,
      url: url,
    };
  }
  const sql = `
          INSERT INTO books(title, author_id, users_id, url)
          VALUES (?, ?, ?, ?);
         `;

  const result = await pool.query(sql, [
    title,
    newAuthor.insertId,
    users_id,
    url,
  ]);

  return {
    id: result.insertId,
    title: title,
    author: author,
    url: url,
  };
};

/**
 * Returns book's status of the book by certain parameter
 *
 * @async
 * @function getBookStatus
 * @param {string} column The name of the column
 * @param {number} value The value, for which will be searched in column
 * @return {Promise<object>} The object with searched data
 */
const getBookStatus = async (column, value) => {
  const sql = `
  select status_id
  from books
  where ${column} = ?;
  `;
  const result = await pool.query(sql, [value]);
  return result;
};

/**
 * Borrows a book by id with status "free"
 *
 * @async
 * @function borrowBook
 * @param {number} bookId The id of the book, which is borrowed
 * @param {number} userId The id of user, who borrows the book
 * @return {Promise<object>} The object with info about the update
 */
const borrowBook = async (bookId, userId) => {
  const sql = `
  update books
  set status_id = 3, users_id = ?, curr_reader_id = ?
  where  id = ?;
  `;
  const result = await pool.query(sql, [userId, userId, bookId]);
  return result;
};

/**
 * Returns a book with given id and sets its status to "free" (1)
 *
 * @async
 * @function returnBook
 * @param {number} bookId The id of the book, which is returned
 * @return {Promise<object>} The object with info about the update
 */
const returnBook = async (bookId) => {
  const sql = `
  update books 
  set status_id = 1, users_id = null, curr_reader_id = null
  where id = ?
  `;
  const result = await pool.query(sql, [bookId]);
  return result;
};

/**
 * Deletes a book with given id and sets its status to "unlisted" (2)
 *
 * @async
 * @function deleteBook
 * @param {number} bookId The id of the book, which is deleted
 * @return {Promise<object>} The object with info about the update
 */
const deleteBook = async (bookId) => {
  const sql = `
  update books b
  set status_id = 2
  where b.id = ?;
  `;
  const result = await pool.query(sql, [bookId]);
  return result;
};

/**
 * Returns a book by a certain parameter
 *
 * @async
 * @function getBookBy
 * @param {string} column The column in which will be searched
 * @param {number} value The value, for which will be searched in column
 * @return {Promise<object>} The object with searched data
 */
const getBookBy = async (column, value) => {
  // console.log('column' + column);
  const sql = `
  select b.id, b.url, a.name as author, b.title, st.status, b.curr_reader_id
  from books b
  join author a
  on a.id = b.author_id
  join status st
  on st.id = b.status_id
  where b.${column} = '${value}'
  and b.status_id != 2;
  `;
  const result = await pool.query(sql, [column, value]);
  return await result[0];
};

/**
 * Returns a rating by a particular book's id
 *
 * @async
 * @function getRatingByBookId
 * @param {number} bookId The value, for which will be searched
 * @return {Promise<object>} The object with searched data
 */
const getRatingByBookId = async (bookId) => {
  const sql = `
  select avg(r.stars) 
  as avgRating 
  from books b
  join ratings r
  on b.id = r.books_id
  where b.id = ?;
  `;
  const result = await pool.query(sql, [bookId]);
  return await result;
};

/**
 * Create a rating
 *
 * @async
 * @function createRatingBy
 * @param {number} star The star given for a book in range [1...5]
 * @param {number} bookId The id of the book for which will be added a review
 * @param {number} userId The id of the user, that adds a rating
 * @return {object} The object with searched data
 */
const createRatingBy = async (star, bookId, userId) => {
  const sql = `
  INSERT INTO ratings(stars, books_id, users_id) 
  VALUES (?, ?, ?);
  `;
  const result = await pool.query(sql, [star, bookId, userId]);
  return {
    star: star,
    books_id: bookId,
    users_id: userId,
  };
};
export default {
  getAll,
  searchBy,
  addAuthor,
  createBookBy,
  getBookStatus,
  borrowBook,
  returnBook,
  deleteBook,
  setReturnedFromUser,
  getBookBy,
  getRatingByBookId,
  createRatingBy
};
