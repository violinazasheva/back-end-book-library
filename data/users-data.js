import pool from './pool.js';

/**
 * Returns user by searched parameter
 *
 * @async
 * @function getBy
 * @param {string} column The column to search in
 * @param {string/number} value The value that will be searched in the column
 * @return {Promise<object>} The object with searched data
 */
const getBy = async (column, value) => {
  const sql = `
  select id, username, is_deleted, ban_status
  from users
  where ${column} = ?;
  `;
  const result = await pool.query(sql, [value]);
  return result[0];
};

/**
 * Creates new user
 *
 * @async
 * @function create
 * @param {string} username The username
 * @param {string} password The password
 * @param {string} role The role of the user (User/Admin)
 * @return {object} The object with id and username of the user
 */
const create = async (username, password, role) => {
  const sql = `
  INSERT INTO users (username, password, roles_id)
  VALUES (?,?,(SELECT id FROM roles WHERE role = ?))
  `;
  const result = await pool.query(sql, [username, password, role]);
  return {
    id: result.insertId,
    username: username
  };
};

/**
 * Gives information about the user by username
 *
 * @async
 * @function getWithRole
 * @param {string} username The username
 * @return {Promise<object>} The object with the needed information
 */
const getWithRole = async (username) => {
  const sql = `
  select u.id, u.username, u.password, r.role, u.is_deleted, u.ban_status
  from users u
  join roles r
  on u.roles_id = r.id
  where u.username = ?;
  `;
  const result = await pool.query(sql, [username]);
  return result[0];
};

/**
 * Changes ban status of the user by given user id and renewed ban status
 *
 * @async
 * @function changeBanStatus
 * @param {number} userId The id of the user, which ban status will be changed
 * @param {number} banStatus The new ban status of the user,
 * which can be either -1 (unbanned) or 1 (banned)
 * @return {Promise<object>} The object with the needed information
 */
const changeBanStatus = async (userId, banStatus) => {
  const sql = `
  update users
  set ban_status = ?
  where id = ?;
  `;
  const result = await pool.query(sql, [banStatus, userId]);
  return result;
};

/**
 * Gives info about the user (id, username, banstatus and role)
 *
 * @async
 * @function getFullInfoById
 * @param {number} userId The id of the user
 * @return {Promise<object>} The object with the needed information
 */
const getFullInfoById = async (userId) => {
  const sql = `
  select u.id, u.username, u.ban_status, r.role
  from users u
  join roles r
  on r.id = u.roles_id
  where u.id = ?;
  `;
  const result = await pool.query(sql, [userId]);
  return result[0];
};

/**
 * Returns id of the user, who is currently reading a book
 *
 * @async
 * @function getCurrReaderId
 * @param {number} currReaderId The id of the user
 * @return {Promise<object>} The object with the needed information
 */
const getCurrReaderId = async (currReaderId) => {
  const sql = `
  select b.curr_reader_id
  from books b
  where curr_reader_id = ?;
  `;
  const result = await pool.query(sql, [+currReaderId]);
  return result[0];
};

/**
 * Sets users property is_deleted to 1
 *
 * @async
 * @function deleteUser
 * @param {number} userId The id of the user
 * @return {Promise<object>} The object with the needed information
 */
const deleteUser = async (userId) => {
  const sql = `
  update users u
  set u.is_deleted = 1
  where u.id = ?;
  `;
  const result = await pool.query(sql, [+userId]);
  return result[0];
};

const allUsers = async () =>{
  const sql = ' SELECT * FROM users;'

  const result = await pool.query(sql);
  return result;
}

export default {
  getBy,
  create,
  getWithRole,
  changeBanStatus,
  getFullInfoById,
  getCurrReaderId,
  deleteUser,
  allUsers
};

