export const DB_CONFIG = {
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: '0000',
  database: 'mydb'
};

export const PRIVATE_KEY = 'the_most_private_key';

export const TOKEN_LIFETIME = 60 * 60;

export const DEFAULT_USER_ROLE = 'User';

export const JWT_STRATEGY_NAME = 'jwt';

export const PORT = 4001;
