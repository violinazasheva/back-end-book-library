import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import { PORT } from './config.js';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';

import booksController from './controllers/books-controller.js';
import usersController from './controllers/users-controller.js';
import authController from './controllers/auth-controller.js';
import reviewsController from './controllers/reviews-controller.js';
import adminController from './controllers/admin-controller.js';
import ratingsController from './controllers/ratings-controller.js';

const app = express();
passport.use(jwtStrategy);

app.use(cors(), bodyParser.json(), helmet());
app.use(passport.initialize());

app.use('/books', booksController, reviewsController, ratingsController);
app.use('/users', usersController);
app.use('/auth', authController);
app.use('/admin', adminController);


app.use((err, req, res, next) => {
  // logger.log(err)

  res.status(500).send({
    message: 'An unexpected error occurred.'
  });
})

app.all('*', (req, res) => {
  res. status(404).send({ message: 'Resource not found!' });
});

app.listen(PORT, () => console.log(`App is listening on port: ${PORT}`));
